[SECTION] Add new records
--> Add 5 artists, 2 albums each, 2 songs per album

--> 5 new artists
INSERT INTO artists (name) VALUES 
("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");


--> 2 albums for tailor swift
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Fearless", "2008-1-1", 3),
("Red", "2012-1-1", 3);

--> 2 songs for fearless album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Fearless", 256, "Pop rock", 5),
("State of Grace", 213, "Rock", 5);

--> 2 songs for red album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Love Story", 312, "Country rock", 6),
("Begin Again", 304, "Country", 6);


--> 2 albums for lady gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("A Star is Born", "2018-1-1", 4),
("Born This Way", "2011-1-1", 4);

--> 2 songs for A Star is Born album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Shallow", 250, "Country, Rock, Folk Rock", 9),
("Always Remember Us This Way", 310, "Country", 9);

--> 2 songs for Born This Way album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Bad Romance", 320, "Electro, Pop", 10),
("Paparazzi", 258, "Pop", 10);


--> Justin B, 1 song per album
--> 2 albums for Justine Bieber
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Purpose", "2015-1-1", 5),
("Believe", "2012-1-1", 5);

--> 1 song for Purpose album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Sorry", 242, "Dancehall-poptropical", 11);

--> 1 song for Believe album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Boyfriend", 320, "Pop", 12);


--> Ariana G, 1 song per album
--> 2 albums for Ariana G
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("Dangerous Woman", "2016-1-1", 6),
("Thank You, Next", "2019-1-1", 6);

--> 1 song for Dangerous Woman album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Into You", 320, "EDM house", 13);

--> 1 song for Thank You, Next album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Thank You, Next",246, "Pop, R&B", 14);


--> Bruno M., 1 song per album
--> 2 albums for Bruno M.
INSERT INTO albums (album_title, date_released, artist_id) VALUES
("24K Magic", "2016-1-1", 7),
("Earth to Mars", "2011-1-1", 7);

--> 1 song for 24K Magic album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("24K Magic", 400, "Funk, Disco, R&B", 15);

--> 1 song for Earth to Mars album
INSERT INTO songs (song_name, length, genre, album_id) VALUES
("Lost", 250, "Pop", 16);


[2] SECTION - Advanced Selection
SELECT * FROM songs WHERE id = 11;

SELECT * FROM songs WHERE id != 11;

SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id <= 11;
SELECT * FROM songs WHERE id > 11;
SELECT * FROM songs WHERE id >= 11;

--> Get specific IDs (OR)
SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

--> Get specific IDs (IN)
SELECT * FROM songs WHERE id IN(1, 3, 5);
SELECT * FROM songs WHERE genre IN("Pop", "K-Pop");

--> Combining conditions
SELECT * FROM songs WHERE album_id = 4 AND id < 8;

--> Find Partial Matches
SELECT * FROM songs WHERE song_name LIKE "%e"; -- select keyword from the end
SELECT * FROM songs WHERE song_name LIKE "e%"; -- select keyword from the start
SELECT * FROM songs WHERE song_name LIKE "%e%"; -- select keyword in or in between

SELECT * FROM albums WHERE date_released LIKE "20_8-01-01"; -- know some keyword

--> Sorting Keywords
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

--> Getting distinct records
SELECT DISTINCT genre FROM song_name; -- remove the duplicate records and make it only 1
SELECT DISTINCT artist_id FROM albums;

SELECT COUNT(artist_id) FROM albums;

[3] TABLE JOINTS
--> Combine artist table and albums table
SELECT * FROM artists 
JOIN albums ON artists.id = albums.artist_id;

-- More than 2 tables
SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

-- Select columns to be included in a table
SELECT artists.name, albums.album_title, songs.song_name FROM artists 
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;


-- Show artists without records on the right sided of the joined table
SELECT * FROM artists
LEFT JOIN albums ON artists.id = albums.artist_id;